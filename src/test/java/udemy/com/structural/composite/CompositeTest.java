package udemy.com.structural.composite;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class CompositeTest {

    @Test
    public void testToTheImplementation() {

        //given
        SingleValue singleValue = new SingleValue(1);
        ManyValues manyValues = new ManyValues();
        manyValues.add(2);
        manyValues.add(3);

        //when and then
        assertEquals(1+2+3, new MyList(Arrays.asList(singleValue,manyValues)).sum());

    }
}

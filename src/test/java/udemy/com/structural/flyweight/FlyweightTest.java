package udemy.com.structural.flyweight;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FlyweightTest {

    @Test
    public void canCapitalizeChosenWord() {
        //given
        Sentence sentence = new Sentence("hello world");

        //when
        sentence.getWord(1).capitalize = true;

        //then
        assertEquals("hello WORLD", sentence.toString());
    }
}

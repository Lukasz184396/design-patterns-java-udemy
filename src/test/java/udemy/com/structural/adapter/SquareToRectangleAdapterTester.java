package udemy.com.structural.adapter;

import org.junit.Test;
import udemy.com.structural.adapter.Square;
import udemy.com.structural.adapter.SquareToRectangleAdapter;

import static junit.framework.TestCase.assertEquals;

public class SquareToRectangleAdapterTester {

    @Test
    public void shouldCountProperlyAreaIfSquareIsAdaptedToRectangle() {
        //given
        Square square = new Square(5);

        //when
        SquareToRectangleAdapter squareToRectangleAdapter = new SquareToRectangleAdapter(square);

        //then
        assertEquals(25, squareToRectangleAdapter.getArea());

    }

}

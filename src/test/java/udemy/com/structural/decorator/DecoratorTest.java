package udemy.com.structural.decorator;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class DecoratorTest {

    @Test
    public void dragonInAgeLessThanTenCanFlyLikeABird() {
        //given and when
        Dragon dragon = new Dragon(1);

        //then
        assertEquals("flying", dragon.fly());
    }

    @Test
    public void dragonInAgeTenOrMoreIsTooOldToFlyLikeABird() {
        //given and when
        Dragon dragon = new Dragon(10);

        //then
        assertEquals("too old", dragon.fly());
    }

    @Test
    public void dragonInAgeLessThanOneIsTooYoungToCrawlingLikeALizard() {
        //given and when
        Dragon dragon = new Dragon(0);

        //then
        assertEquals("too young", dragon.crawl());
    }

    @Test
    public void dragonInAgeOneOrMoreCanCrawlingLikeALizard() {
        //given and when
        Dragon dragon = new Dragon(1);

        //then
        assertEquals("too young", dragon.crawl());
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenDragonIsInAgeLessThanZeroIllegalArgumentExceptionIsExpected() {
        //given
        Dragon dragon = new Dragon(10);

        //when
        dragon.setAge(-1);

        //then
        //IllegalArgumentException.class expected
    }

}

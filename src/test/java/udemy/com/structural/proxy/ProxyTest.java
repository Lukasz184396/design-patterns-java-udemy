package udemy.com.structural.proxy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProxyTest {

    @Test
    public void personOverSixteenCanDriveACar() {
        //given and when
        Person person = new ResponsiblePerson(new Person(16));

        //then
        assertEquals("driving", person.drive());
    }

    @Test
    public void personUnderSixteenIsTooYoungToDriveACar() {
        //given and when
        Person person = new ResponsiblePerson(new Person(15));

        //then
        assertEquals("too young", person.drive());
    }

    @Test
    public void personOverSeventeenCanDrinkAlcohol() {
        //given and when
        Person person = new ResponsiblePerson(new Person(18));

        //then
        assertEquals("drinking", person.drink());
    }

    @Test
    public void personUnderEighteenIsTooYoungToDrinkAlcohol() {
        //given and when
        Person person = new ResponsiblePerson(new Person(17));

        //then
        assertEquals("too young", person.drink());
    }

    @Test
    public void drivingWhileDrinkingCausePersonDead() {
        //given and when
        Person person = new ResponsiblePerson(new Person(17));

        //then
        assertEquals("dead", person.drinkAndDrive());
    }

    @Test
    public void shouldReturnSuccessForMultipleConditions()
    {
        Person p = new ResponsiblePerson(new Person(14));

        assertEquals("too young", p.drive());
        assertEquals("too young", p.drink());
        assertEquals("dead", p.drinkAndDrive());

        p.setAge(100);

        assertEquals("driving", p.drive());
        assertEquals("drinking", p.drink());
        assertEquals("dead", p.drinkAndDrive());
    }
}

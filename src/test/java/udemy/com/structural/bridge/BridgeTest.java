package udemy.com.structural.bridge;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class BridgeTest {

    @Test
    public void shouldReturnSuccessWhenNewSquareIsCreatedUsingRasterRenderer(){
        assertEquals("Drawing Square as pixels", new Square(new RasterRenderer()).toString());
    }

    @Test
    public void shouldReturnSuccessWhenNewSquareIsCreatedUsingVectorRenderer(){
        assertEquals("Drawing Square as lines", new Square(new VectorRenderer()).toString());
    }

    @Test
    public void shouldReturnSuccessWhenNewTriangleIsCreatedUsingRasterRenderer(){
        assertEquals("Drawing Triangle as pixels", new Triangle(new RasterRenderer()).toString());
    }

    @Test
    public void shouldReturnSuccessWhenNewTriangleIsCreatedUsingVectorRenderer(){
        assertEquals("Drawing Triangle as lines", new Triangle(new VectorRenderer()).toString());
    }

}

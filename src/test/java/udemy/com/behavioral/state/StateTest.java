package udemy.com.behavioral.state;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StateTest {
    
    @Test
    public void canUnlockWhenCombinationIsCorrect()
    {
        CombinationLock combinationLock = new CombinationLock(new int[]{1, 2, 3, 4});
        assertEquals("LOCKED", combinationLock.status);

        combinationLock.enterDigit(1);
        assertEquals("1", combinationLock.status);

        combinationLock.enterDigit(2);
        assertEquals("12", combinationLock.status);

        combinationLock.enterDigit(3);
        assertEquals("123", combinationLock.status);

        combinationLock.enterDigit(4);
        assertEquals("OPEN", combinationLock.status);
    }

    @Test
    public void statusIsErrorWhenCombinationIsBad()
    {
        CombinationLock combinationLock = new CombinationLock(new int[]{1, 2, 3});

        assertEquals("LOCKED", combinationLock.status);

        combinationLock.enterDigit(1);
        assertEquals("1", combinationLock.status);

        combinationLock.enterDigit(2);
        assertEquals("12", combinationLock.status);

        combinationLock.enterDigit(5);
        assertEquals("ERROR", combinationLock.status);
    }


}

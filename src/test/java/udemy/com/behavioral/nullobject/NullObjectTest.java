package udemy.com.behavioral.nullobject;

import org.junit.Test;

public class NullObjectTest {

    @Test(expected = Exception.class)
    public void test() throws Exception {
        //given
        Log log = new NullLog();
        Account account = new Account(log);

        //when
        account.someOperation();  //then expected Exception vf
    }

}

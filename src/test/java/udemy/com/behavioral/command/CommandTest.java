package udemy.com.behavioral.command;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class CommandTest {

    @Test
    public void  canDepositMoneyToAccount() {
        //given
        Command command = new Command(Command.Action.DEPOSIT, 100 );
        Account account = new Account();

        //when
        account.process(command);

        //then
        assertEquals(100, account.balance);
    }

    @Test
    public void  canNotWithdrawMoneyFromAccount() {
        //given
        Command withdrawCommand = new Command(Command.Action.WITHDRAW, 100 );
        Account account = new Account();

        //when
        account.process(withdrawCommand);

        //then
        assertEquals(0, account.balance);
        assertFalse(withdrawCommand.success);
    }

    @Test
    public void  canDepositAndWithdrawMoneyFromAccount() {
        //given
        Command depositCommand = new Command(Command.Action.DEPOSIT, 1000 );
        Account account = new Account();

        //when
        account.process(depositCommand);

        //then
        assertEquals(1000, account.balance);
        assertTrue(depositCommand.success);

        // and when
        Command withdrawCommand = new Command(Command.Action.WITHDRAW, 600 );
        account.process(withdrawCommand);

        //then
        assertEquals(400, account.balance);
        assertTrue(withdrawCommand.success);
    }


}

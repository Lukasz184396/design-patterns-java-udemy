package udemy.com.behavioral.observer;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ObserverTest {

    @Test
    public void ratCanAttackAPlayer() {
        //given
        Game game = new Game();

        //when
        Rat rat = new Rat(game);

        //then
        assertEquals(1,rat.attack);
    }

    @Test
    public void ratSwarmCanAttackAPlayerWithCumulativePower() {
        //given
        Game game = new Game();

        //when
        List<Rat> ratSwarm = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Rat rat = new Rat(game);
            ratSwarm.add(rat);
        }

        //then
        assertEquals(10,ratSwarm.get(0).attack);
    }

    @Test
    public void ratCanDie() throws IOException {
        //given
        Game game = new Game();

        //when
        Rat rat = new Rat(game);
        rat.close();

        //then
        assertEquals(0,rat.attack);     //dead rat cannot attack
    }
}

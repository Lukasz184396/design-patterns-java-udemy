package udemy.com.behavioral.memento;

import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class MementoTest {

    @Test
    public void tokenMachineCanAddToken() {
        //given
        TokenMachine tokenMachine = new TokenMachine();

        //when
        Memento memento = tokenMachine.addToken(5);

        //then
        assertEquals(1, tokenMachine.tokens.size());
    }

    @Test
    public void tokenMachineCanAddTokenAndRevertIt() {
        //given
        TokenMachine tokenMachine = new TokenMachine();

        //when
        Memento memento = tokenMachine.addToken(5);
        tokenMachine.addToken(100);
        tokenMachine.revert(memento);

        //then
        assertEquals(1, tokenMachine.tokens.size());
        assertEquals(5, tokenMachine.tokens.get(0).value);
    }
    @Test
    public void twoTokenTest()
    {
        //given
        TokenMachine tm = new TokenMachine();
        tm.addToken(1);
        Memento m = tm.addToken(2);
        tm.addToken(3);
        tm.revert(m);
        assertEquals(2, tm.tokens.size());
        assertEquals("First token should have value 1",
                1, tm.tokens.get(0).value);
        assertEquals(2, tm.tokens.get(1).value);
    }

    /*
     SCENARIO ONE

     Token is fed in as a reference
     and its value is subsequently changed on that reference
     return the correct system snapshot
    */
    @Test
    public void scenarioOne()
    {
        TokenMachine tm = new TokenMachine();
        System.out.println("Made a token with value 111 and kept a reference");
        Token token = new Token(111);
        System.out.println("Added this token to the list");
        tm.addToken(token);
        Memento m = tm.addToken(222);
        System.out.println("Changed this token's value to 333 :)");
        token.value = 333;
        tm.revert(m);

        assertEquals(
                "At this point, token machine should have exactly two tokens, "
                        + "but you got " + tm.tokens.size(),
                2, tm.tokens.size()
        );

        assertEquals("You got the token value wrong here. " +
                        "Hint: did you init the memento by value?",
                111, tm.tokens.get(0).value);
    }
}

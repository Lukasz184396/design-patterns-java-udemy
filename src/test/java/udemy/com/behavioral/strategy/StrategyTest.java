package udemy.com.behavioral.strategy;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;


public class StrategyTest {

    @Test
    public void calculateDiscriminantUsingOrdinaryDiscriminantStrategyTestWhenNegativeDiscriminant() {

        //given
        double a = 3 , b = 4, c =3;
        DiscriminantStrategy discriminantStrategy = new OrdinaryDiscriminantStrategy();

        //when
        double discriminantCalculated = discriminantStrategy.calculateDiscriminant(a,b,c);

        //then
        assertEquals(-20d ,discriminantCalculated);
    }

    @Test
    public void calculateDiscriminantUsingRealDiscriminantStrategyTestWhenNegativeDiscriminant() {

        //given
        double a = 3 , b = 4, c =3;
        DiscriminantStrategy discriminantStrategy = new RealDiscriminantStrategy();

        //when
        double discriminantCalculated = discriminantStrategy.calculateDiscriminant(a,b,c);

        //then
        assertEquals(Double.NaN ,discriminantCalculated);
    }

    @Test
    public void positiveTestOrdinaryStrategy()
    {
        DiscriminantStrategy strategy = new OrdinaryDiscriminantStrategy();
        QuadraticEquationSolver solver = new QuadraticEquationSolver(strategy);
        Pair<Complex, Complex> results = solver.solve(1, 10, 16);
        assertEquals(new Complex(-2, 0), results.first);
        assertEquals(new Complex(-8, 0), results.second);
    }

    @Test
    public void positiveTestRealStrategy()
    {
        DiscriminantStrategy strategy = new RealDiscriminantStrategy();
        QuadraticEquationSolver solver = new QuadraticEquationSolver(strategy);
        Pair<Complex, Complex> results = solver.solve(1, 10, 16);
        assertEquals(new Complex(-2, 0), results.first);
        assertEquals(new Complex(-8, 0), results.second);
    }

    @Test
    public void negativeTestRealStrategy()
    {
        DiscriminantStrategy strategy = new RealDiscriminantStrategy();
        QuadraticEquationSolver solver = new QuadraticEquationSolver(strategy);
        Pair<Complex, Complex> results = solver.solve(1, 4, 5);

        assertTrue(results.first.isNaN());
        assertTrue(results.second.isNaN());
    }

    @Test
    public void negativeTestOrdinaryStrategy()
    {
        OrdinaryDiscriminantStrategy strategy = new OrdinaryDiscriminantStrategy();
        QuadraticEquationSolver solver = new QuadraticEquationSolver(strategy);
        Pair<Complex, Complex> results = solver.solve(1, 4, 5);
        assertEquals(new Complex(-2, 1), results.first);
        assertEquals(new Complex(-2, -1), results.second);
    }


}

package udemy.com.behavioral.mediator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MediatorTest {

    @Test
    public void participantCanBroadcastValueToAllOtherParticipant() {
        //given
        Mediator mediator = new Mediator();
        Participant participant1 = new Participant(mediator);
        Participant participant2 = new Participant(mediator);

        //when
        participant1.say(2);
        participant2.say(4);

        //then
        assertEquals(4, participant1.value);
        assertEquals(2, participant2.value);
    }
}

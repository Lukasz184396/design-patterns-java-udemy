package udemy.com.behavioral.visitor;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VisitorTest {

    @Test
    public void simpleAddition() {
        AdditionExpression simple = new AdditionExpression(new Value(2), new Value(3));
        ExpressionPrinter ep = new ExpressionPrinter();
        ep.visit(simple);
        assertEquals("(2+3)", ep.toString());
    }


    @Test
    public void shouldReturnAdditionAndValue() {
        MultiplicationExpression expr = new MultiplicationExpression(
                new AdditionExpression(new Value(1), new Value(2)),
                new Value(3)
        );
        ExpressionPrinter ep = new ExpressionPrinter();
        ep.visit(expr);
        assertEquals("(1+2)*3", ep.toString());
    }
}

package udemy.com.behavioral.chainofresponsibility;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChainOfResponsibilityTest {

    @Test
    public void whenOnlyOneGoblinIsInGameHisAttackAndDefenceShouldEqualsOne() {
        //given
        Game game = new Game();
        Goblin goblin = new Goblin(game);

        //when
        game.creatures.add(goblin);

        //then
        assertEquals(1, goblin.getAttack());
        assertEquals(1, goblin.getDefense());
    }

    @Test
    public void whenOnlyOneGoblinKingIsInGameHisAttackAndDefenceShouldEqualsThree() {
        //given
        Game game = new Game();
        Goblin goblin = new GoblinKing(game);

        //when
        game.creatures.add(goblin);

        //then
        assertEquals(3, goblin.getAttack());
        assertEquals(3, goblin.getDefense());
    }

    @Test
    public void whenGoblinKingIsAddedToGameOthersGoblinAttackIsIncreaseByOne() {
        //given
        Game game = new Game();
        Goblin goblin = new Goblin(game);
        Goblin goblin2 = new Goblin(game);
        Goblin goblinKing = new GoblinKing(game);
        game.creatures.add(goblin);
        game.creatures.add(goblin2);

        //when
        game.creatures.add(goblinKing);

        //then
        assertEquals(2, goblin.getAttack());
        assertEquals(2, goblin2.getAttack());
    }


    @Test
    public void whenGoblinIsAddedToGameOthersGoblinDefenceIsIncreaseByOne() {
        //given
        Game game = new Game();
        Goblin goblin = new Goblin(game);
        Goblin goblin2 = new Goblin(game);
        Goblin goblinKing = new GoblinKing(game);
        game.creatures.add(goblin);
        game.creatures.add(goblin2);

        //when
        game.creatures.add(goblinKing);

        //then
        assertEquals(3, goblin.getDefense());
        assertEquals(3, goblin2.getDefense());
        assertEquals(5, goblinKing.getDefense());
    }
}

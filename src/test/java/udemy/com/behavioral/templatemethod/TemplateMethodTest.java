package udemy.com.behavioral.templatemethod;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TemplateMethodTest {

    @Test
    public void nobodyWinsTemporaryCardDamageGame() {
        //given
        Creature creature1 = new Creature(20, 100);
        Creature creature2 = new Creature(20, 100);
        CardGame cardGame = new TemporaryCardDamageGame(new Creature[]{creature1, creature2});

        //when and then
        assertEquals(-1, cardGame.combat(0, 1));
    }

    @Test
    public void nobodyWinsPermanentCardDamageGame() {
        //given
        Creature creature1 = new Creature(20, 100);
        Creature creature2 = new Creature(20, 100);
        CardGame cardGame = new PermanentCardDamageGame(new Creature[]{creature1, creature2});

        //when and then
        assertEquals(-1, cardGame.combat(0, 1));
        assertEquals(-1, cardGame.combat(0, 1));
    }

    @Test
    public void temporaryMurderTest() {
        Creature creature1 = new Creature(1, 1);
        Creature creature2 = new Creature(2, 2);
        TemporaryCardDamageGame game = new TemporaryCardDamageGame(new Creature[]{creature1, creature2});
        assertEquals(1, game.combat(0, 1));
    }

    @Test
    public void doubleMurderTest() {
        Creature creature1 = new Creature(2, 2);
        Creature creature2 = new Creature(2, 2);
        TemporaryCardDamageGame game = new TemporaryCardDamageGame(new Creature[]{creature1, creature2});
        assertEquals(-1, game.combat(0, 1));
    }

    @Test
    public void permanentDamageDeathTest() {
        Creature creature1 = new Creature(1, 2);
        Creature creature2 = new Creature(1, 3);
        CardGame game = new PermanentCardDamageGame(new Creature[]{creature1, creature2});
        assertEquals(-1, game.combat(0, 1));
        assertEquals(1, game.combat(0, 1));
    }
}
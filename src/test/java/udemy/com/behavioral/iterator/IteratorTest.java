package udemy.com.behavioral.iterator;

import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

public class IteratorTest {

    @Test
    public void preOrderTraversalTest() {
        //given
        Node<Character> node = new Node<>('1',
                new Node<>('2',
                        new Node<>('3'),
                        new Node<>('4')),
                new Node<>('5'));

        //when
        StringBuilder stringBuilder = new StringBuilder();
        Iterator<Node<Character>> it = node.preOrder();
        while (it.hasNext()) {
            stringBuilder.append(it.next().value);
        }

        //then
        assertEquals("12345", stringBuilder.toString());
    }
}

package udemy.com.creational.singleton;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class SingletonTest {

    @Test
    public void shouldCheckIfSingletonTesterIsOK()
    {
        Object object = new Object();
        assertTrue(SingletonTester.isSingleton(() -> object));
        assertFalse(SingletonTester.isSingleton(()-> new Object()));
    }

}

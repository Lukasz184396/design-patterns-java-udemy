package udemy.com.creational.prototype;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LineTest {

    @Test
    public void shouldReturnCopyOfLine() {
        //given
        Line line = new Line(new Point(0, 0), new Point(10, 10));

        //when
        Line line1 = line;

        //then
        assertEquals(0, line1.start.x);
        assertEquals(0, line1.start.y);

        assertEquals(10, line1.end.x);
        assertEquals(10, line1.end.y);
    }

    @Test
    public void shouldReturnCopyOfLineWithDifferentEnd() {

        //given
        Line line = new Line(new Point(0, 0), new Point(10, 10));

        //when
        Line line1 = line.deepCopy();
        line1.end.x=5;
        line1.end.y=5;

        //then
        assertEquals(0, line.start.x);
        assertEquals(0, line.start.y);
        assertEquals(10, line.end.x);
        assertEquals(10, line.end.y);

        assertEquals(0, line1.start.x);
        assertEquals(0, line1.start.y);
        assertEquals(5, line1.end.x);
        assertEquals(5, line1.end.y);
    }

    @Test
    public void shouldReturnCopyOfLineWithDifferentStart() {

        //given
        Line line = new Line(new Point(0, 0), new Point(10, 10));

        //when
        Line line1 = line.deepCopy();
        line1.start.x=55;
        line1.start.y=55;

        //then
        assertEquals(0, line.start.x);
        assertEquals(0, line.start.y);
        assertEquals(10, line.end.x);
        assertEquals(10, line.end.y);

        assertEquals(55, line1.start.x);
        assertEquals(55, line1.start.y);
        assertEquals(10, line1.end.x);
        assertEquals(10, line1.end.y);
    }
}

package udemy.com.creational.factory;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersonFactoryTest {

    @Test
    public void shouldReturnNewInstanceOfPerson() {

        //given
        PersonFactory personFactory = new PersonFactory();

        //when
        Person person = personFactory.createPerson("Ala");

        //then
        assertEquals(0, person.id);
        assertEquals("Ala", person.name);
    }

    @Test
    public void shouldIncrementIdWhenNextPeopleAreCreated() {

        //given
        PersonFactory personFactory = new PersonFactory();

        //when
        Person person = personFactory.createPerson("Ala");
        Person person2 = personFactory.createPerson("Jan");
        Person person3 = personFactory.createPerson("Tom");

        //then
        assertEquals(0, person.id);
        assertEquals("Ala", person.name);

        assertEquals(1, person2.id);
        assertEquals("Jan", person2.name);

        assertEquals(2, person3.id);
        assertEquals("Tom", person3.name);
    }

}

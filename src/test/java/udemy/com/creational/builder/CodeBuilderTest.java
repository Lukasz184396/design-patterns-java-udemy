package udemy.com.creational.builder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CodeBuilderTest {

    @Test
    public void shouldCreatePublicClassPersonWithoutContent() {

        //given and when
        CodeBuilder codeBuilder = new CodeBuilder("Person");

        //then
        assertEquals("public class Person\n{\n}",
                     codeBuilder.toString());
    }

    @Test
    public void shouldCreatePersonClassWithNameAndAgeProperties()
    {
        //given and when
        CodeBuilder codeBuilder = new CodeBuilder("Person")
                .addField("name", "String")
                .addField("age", "int");

        //then
        assertEquals("public class Person\n{\n" +
                              "  public String name;\n" +
                              "  public int age;\n}",
                     codeBuilder.toString());
    }

    @Test
    public void shouldCreatePersonClassWithNameProperty()
    {
        //given and when
        CodeBuilder codeBuilder = new CodeBuilder("Person")
                .addField("name", "String");

        //then
        assertEquals("public class Person\n{\n" +
                             "  public String name;\n}",
                     codeBuilder.toString());
    }
}

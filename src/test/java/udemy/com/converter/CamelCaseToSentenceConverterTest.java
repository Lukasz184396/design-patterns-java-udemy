package udemy.com.converter;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CamelCaseToSentenceConverterTest {

    @Test
    public void canCovertCamelCaseToSentence() {
        //given
        String camelCaseString = "alaMaKota";
        String expectedSentenceFromCamelCase = "ala ma kota";

        //when
        String actualSentenceFromCamelCase  = CamelCaseToSentenceConverter.convert(camelCaseString);

        //then
        assertEquals(expectedSentenceFromCamelCase, actualSentenceFromCamelCase);
    }
}

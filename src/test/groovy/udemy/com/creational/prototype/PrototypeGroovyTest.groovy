package udemy.com.creational.prototype

import spock.lang.Specification



class PrototypeGroovyTest extends Specification{

    def "should return copy of line" () {
        given:
        Line line = new Line(new Point(0, 0), new Point(10, 10))

        when:
        Line line1 = line

        then:
        0 == line1.start.x
        0 == line1.start.y

        10 == line1.end.x
        10 == line1.end.y
    }

    def "should return copy of line with different end"() {

        given:
        Line line = new Line(new Point(0, 0), new Point(10, 10))

        when:
        Line line1 = line.deepCopy();
        line1.end.x=5;
        line1.end.y=5;

        then:
        0 == line.start.x
        0 == line.start.y
        10 == line.end.x
        10 == line.end.y

        0 == line1.start.x
        0 == line1.start.y
        5 == line1.end.x
        5 == line1.end.y
    }

   def "should return copy of line with different start"() {

        given:
        Line line = new Line(new Point(0, 0), new Point(10, 10))

        when:
        Line line1 = line.deepCopy()
        line1.start.x=55
        line1.start.y=55

        then:
        0 == line.start.x
        0 == line.start.y
        10 == line.end.x
        10 == line.end.y

        55 == line1.start.x
        55 == line1.start.y
        10 == line1.end.x
        10 == line1.end.y
    }
}

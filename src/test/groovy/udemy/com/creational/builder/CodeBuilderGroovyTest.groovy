package udemy.com.creational.builder

import spock.lang.Specification


class CodeBuilderGroovyTest extends Specification {

    def "should Create Public Class Person Without Content"() {

        when:
        CodeBuilder codeBuilder = new CodeBuilder("Person")

        then:
        "public class Person\n{\n}" == codeBuilder.toString()
    }

    def "should create person class with name and age properties"() {
        when:
        CodeBuilder codeBuilder = new CodeBuilder("Person")
                .addField("name", "String")
                .addField("age", "int")

        then:
        "public class Person\n{\n" +
                "  public String name;\n" +
                "  public int age;\n}" == codeBuilder.toString()
    }

   def "should create person class with name property" ()
    {
        when:
        CodeBuilder codeBuilder = new CodeBuilder("Person")
                .addField("name", "String")

        then:
        "public class Person\n{\n" +
            "  public String name;\n}" == codeBuilder.toString()
    }

}





package udemy.com.creational.factory

import spock.lang.Specification

class PersonFactoryGroovyTest extends Specification {

    def "person factory should return new instance of person" () {
        given:
        PersonFactory personFactory = new PersonFactory()

        when:
        Person person = personFactory.createPerson("Ala")

        then:
        0 == person.id
        "Ala" == person.name
    }

    def "person factory should increment id when next person is created" () {

        given:
        PersonFactory personFactory = new PersonFactory();

        when:
        Person person = personFactory.createPerson("Ala")
        Person person2 = personFactory.createPerson("Jan")
        Person person3 = personFactory.createPerson("Tom")

        then:
        0 == person.id
        "Ala" == person.name

        1 == person2.id
        "Jan" == person2.name

        2 == person3.id
        "Tom" == person3.name
    }
}

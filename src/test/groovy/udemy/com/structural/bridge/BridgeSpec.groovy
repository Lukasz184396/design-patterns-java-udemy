package udemy.com.structural.bridge

import spock.lang.Specification

class BridgeSpec extends Specification {

    def "should return success when new square is created using raster renderer"(){
        when:
        def actual = new Square(new RasterRenderer()).toString()

        then:
        "Drawing Square as pixels" == actual
    }

    def "should return success when new square is created using vector renderer"(){
        when:
        def actual = new Square(new VectorRenderer()).toString()

        then:
        "Drawing Square as lines" == actual
    }

    def "should return success when new triangle is created using raster renderer"(){
        when:
        def actual = new Triangle(new RasterRenderer()).toString()

        then:
        "Drawing Triangle as pixels" == actual
    }

    def "should return success when new triangle is created using vector renderer"(){
        when:
        def actual = new Triangle(new VectorRenderer()).toString()

        then:
        "Drawing Triangle as lines" == actual
    }
}

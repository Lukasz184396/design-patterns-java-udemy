package udemy.com.structural.adapter

import spock.lang.Specification

class AdapterSpec extends Specification {

    def "should count properly area if square is adapted to rectangle"() {

        given:
        Square square = new Square(5)

        when:
        SquareToRectangleAdapter squareToRectangleAdapter = new SquareToRectangleAdapter(square)

        then:
        25 == squareToRectangleAdapter.getArea()
    }
}

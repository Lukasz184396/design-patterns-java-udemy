package udemy.com.structural.composite

import spock.lang.Specification

class CompositeSpec extends Specification {

    def testToTheImplementation() {

        given:
        SingleValue singleValue = new SingleValue(1)
        ManyValues manyValues = new ManyValues()
        manyValues.add(2)
        manyValues.add(3)

        when:
        def actual = new MyList(Arrays.asList(singleValue,manyValues)).sum()

        then:
        1+2+3 == actual
    }
}

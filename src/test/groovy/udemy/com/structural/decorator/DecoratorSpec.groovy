package udemy.com.structural.decorator

import spock.lang.Specification

class DecoratorSpec extends Specification {

    def "dragon in ageLess than ten can fly like a bird" () {
        given:
        Dragon dragon = new Dragon(1)

        when:
        String actual = dragon.fly()

        then:
        "flying" == actual
    }

    def dragonInAgeTenOrMoreIsTooOldToFlyLikeABird() {
        given:
        Dragon dragon = new Dragon(10)

        when:
        String actual = dragon.fly()

        then:
        "too old" == actual
    }


    def "dragon in age less than one is too young to crawling like a lizard"() {
        given:
        Dragon dragon = new Dragon(0)

        when:
        String actual = dragon.crawl()

        then:
        "too young" == actual
    }


    def "dragon in age one or more can crawling like a lizard"() {
        given:
        Dragon dragon = new Dragon(1)

        when:
        String actual = dragon.crawl()

        then:
        "too young" == actual
    }

    def "when dragon is in ageLess than zero illegal argument exception is expected"() {
        given:
        Dragon dragon = new Dragon(10);

        when:
        dragon.setAge(-1);

        then:
        thrown(IllegalArgumentException)
    }
}

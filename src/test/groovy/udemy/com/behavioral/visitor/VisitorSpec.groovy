package udemy.com.behavioral.visitor

import spock.lang.Specification

class VisitorSpec extends Specification {

    def simpleAddition() {
        given:
        AdditionExpression simple = new AdditionExpression(new Value(2), new Value(3))
        ExpressionPrinter ep = new ExpressionPrinter()

        when:
        ep.visit(simple)

        then:
        "(2+3)" == ep.toString()
    }

    void "should return addition and value"() {
        given:
        MultiplicationExpression expr = new MultiplicationExpression(
                new AdditionExpression(new Value(1), new Value(2)),
                new Value(3)
        )
        ExpressionPrinter ep = new ExpressionPrinter()

        when:
        ep.visit(expr)

        then:
        "(1+2)*3" == ep.toString()
    }
}

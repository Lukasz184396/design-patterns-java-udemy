package udemy.com.behavioral.mediator

import spock.lang.Specification

class MediatorSpec extends Specification {

     def "participant can broadcast value to all other participant"() {
        given:
        Mediator mediator = new Mediator()
        Participant participant1 = new Participant(mediator)
        Participant participant2 = new Participant(mediator)

        when:
        participant1.say(2)
        participant2.say(4)

        then:
        4 ==participant1.value
        2 == participant2.value
    }
}

package udemy.com.behavioral.iterator

import spock.lang.Specification

class IteratorSpec extends Specification {

    def "pre order traversal test"() {
        given:
        Node<Character> node = new Node<>('1',
                new Node<>('2',
                        new Node<>('3'),
                        new Node<>('4')),
                new Node<>('5'))

        when:
        StringBuilder stringBuilder = new StringBuilder()
        Iterator<Node<Character>> it = node.preOrder()
        while (it.hasNext()) {
            stringBuilder.append(it.next().value)
        }

        then:
        "12345" == stringBuilder.toString()
    }
}

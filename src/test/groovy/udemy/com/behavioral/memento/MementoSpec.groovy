package udemy.com.behavioral.memento

import spock.lang.Specification

class MementoSpec extends Specification {

    def "token machine can add token"() {
        given:
        TokenMachine tokenMachine = new TokenMachine()

        when:
        Memento memento = tokenMachine.addToken(5)

        then:
        1 == tokenMachine.tokens.size()
    }

    def "token machine can add token and revert it"() {
        given:
        TokenMachine tokenMachine = new TokenMachine()

        when:
        Memento memento = tokenMachine.addToken(5)
        tokenMachine.addToken(100)
        tokenMachine.revert(memento)

        then:
        1 == tokenMachine.tokens.size()
        5 == tokenMachine.tokens.get(0).value
    }

    def "two token test"() {
        given:
        TokenMachine tokenMachine = new TokenMachine();

        when:
        tokenMachine.addToken(1)
        Memento memento = tokenMachine.addToken(2)
        tokenMachine.addToken(3)
        tokenMachine.revert(memento)

        then:
        2 == tokenMachine.tokens.size()
        1 == tokenMachine.tokens.get(0).value
        2 == tokenMachine.tokens.get(1).value
    }


    /*
     SCENARIO ONE

     Token is fed in as a reference
     and its value is subsequently changed on that reference
     return the correct system snapshot
    */

    def "scenario one"() {

        when:
            TokenMachine tokenMachine = new TokenMachine()

            System.out.println("Made a token with value 111 and kept a reference")
            Token token = new Token(111)

            System.out.println("Added this token to the list")
            tokenMachine.addToken(token)
            Memento memento = tokenMachine.addToken(222)

            System.out.println("Changed this token's value to 333 :)")
            token.value = 333

            tokenMachine.revert(memento)

        then:
            2   == tokenMachine.tokens.size()
            111 == tokenMachine.tokens.get(0).value
    }

}

package udemy.com.behavioral.state

import spock.lang.Specification

class StateSpec extends Specification {

    def "can unlock when combination is correct"() {

        given:
        def unlockingCombination = [1, 2, 3, 4] as int[]

        when:
        CombinationLock combinationLock = new CombinationLock(unlockingCombination)
        then:
        "LOCKED" == combinationLock.status

        when:
        combinationLock.enterDigit(1)
        then:
        "1" == combinationLock.status

        when:
        combinationLock.enterDigit(2)
        then:
        "12" == combinationLock.status

        when:
        combinationLock.enterDigit(3)
        then:
        "123" == combinationLock.status

        when:
        combinationLock.enterDigit(4)
        then:
        "OPEN" == combinationLock.status
    }


    def "status is error when combination is bad"() {

        given:
        def unlockingCombination = [1, 2, 3, 4] as int[]

        when:
        CombinationLock combinationLock = new CombinationLock(unlockingCombination)
        then:
        "LOCKED" == combinationLock.status

        when:
        combinationLock.enterDigit(1)
        then:
        "1" == combinationLock.status

        when:
        combinationLock.enterDigit(2)
        then:
        "12" == combinationLock.status

        when:
        combinationLock.enterDigit(5)
        then:
        "ERROR" == combinationLock.status
    }

}

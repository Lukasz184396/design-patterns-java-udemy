package udemy.com.behavioral

import spock.lang.Specification
import udemy.com.behavioral.chainofresponsibility.Game
import udemy.com.behavioral.chainofresponsibility.Goblin
import udemy.com.behavioral.chainofresponsibility.GoblinKing

class ChainOfResponsibilitySpec extends Specification {

    def "when only one goblin is in game his attack and defence should equals one"() {
        given:
        Game game = new Game()
        Goblin goblin = new Goblin(game)

        when:
        game.creatures.add(goblin)

        then:
        1 == goblin.getAttack()
        1 == goblin.getDefense()
    }


    def "when only one goblin king is in game his attack and defence should qquals three"() {
        given:
        Game game = new Game();
        Goblin goblin = new GoblinKing(game)

        when:
        game.creatures.add(goblin)

        then:
        3 == goblin.getAttack()
        3 == goblin.getDefense()
    }

    def void "when goblin king is added to game others goblin attack is increase by one"() {
        given:
        Game game = new Game()
        Goblin goblin = new Goblin(game)
        Goblin goblin2 = new Goblin(game)
        Goblin goblinKing = new GoblinKing(game)
        game.creatures.add(goblin)
        game.creatures.add(goblin2)

        when:
        game.creatures.add(goblinKing)

        then:
        2 == goblin.getAttack()
        2 == goblin2.getAttack()
    }

    def "when goblin is added to game others goblin defence is increase by one"() {
        given:
        Game game = new Game()
        Goblin goblin = new Goblin(game)
        Goblin goblin2 = new Goblin(game)
        Goblin goblinKing = new GoblinKing(game)
        game.creatures.add(goblin)
        game.creatures.add(goblin2)

        when:
        game.creatures.add(goblinKing)

        then:
        3 == goblin.getDefense()
        3 == goblin2.getDefense()
        5 == goblinKing.getDefense()
    }

}

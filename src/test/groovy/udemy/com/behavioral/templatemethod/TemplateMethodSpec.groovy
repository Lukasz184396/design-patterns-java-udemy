package udemy.com.behavioral.templatemethod

import spock.lang.Specification

class TemplateMethodSpec extends Specification {

    def "nobody wins temporary card damage game"() {

        given:
        Creature creature1 = new Creature(20, 100)
        Creature creature2 = new Creature(20, 100)
        Creature[] fightingCreatures = [creature1,creature2].toArray()
        CardGame cardGame = new TemporaryCardDamageGame(fightingCreatures)

        when:
        int winner = cardGame.combat(0, 1)

        then:
        -1 == winner
    }

    def "nobody wins permanent card damage game"() {

        given:
        Creature creature1 = new Creature(20, 100)
        Creature creature2 = new Creature(20, 100)
        Creature[] fightingCreatures = [creature1,creature2].toArray()
        CardGame cardGame = new PermanentCardDamageGame(fightingCreatures)

        when:
        int firstFightWinner = cardGame.combat(0, 1)
        int secondFightWinner = cardGame.combat(0, 1)

        then:
        -1 == firstFightWinner
        -1 == secondFightWinner
    }

    def temporaryMurderTest() {

        given:
        Creature creature1 = new Creature(1, 1)
        Creature creature2 = new Creature(2, 2)
        Creature[] fightingCreatures = [creature1,creature2].toArray()
        TemporaryCardDamageGame game = new TemporaryCardDamageGame(fightingCreatures)

        when:
        int winner = game.combat(0, 1)

        then:
        1 == winner
    }

    def doubleMurderTest() {

        given:
        Creature creature1 = new Creature(2, 2)
        Creature creature2 = new Creature(2, 2)
        Creature[] fightingCreatures = [creature1,creature2].toArray()
        TemporaryCardDamageGame game = new TemporaryCardDamageGame(fightingCreatures)

        when:
        int winner = game.combat(0, 1)

        then:
        -1 == winner
    }
    def permanentDamageDeathTest() {

        given:
        Creature creature1 = new Creature(1, 2)
        Creature creature2 = new Creature(1, 3)
        Creature[] fightingCreatures = [creature1,creature2].toArray()
        CardGame game = new PermanentCardDamageGame(fightingCreatures)

        when:
        int firstFightWinner = game.combat(0, 1)
        int secondFightWinner = game.combat(0, 1)

        then:
        -1 == firstFightWinner
         1 == secondFightWinner
    }
}

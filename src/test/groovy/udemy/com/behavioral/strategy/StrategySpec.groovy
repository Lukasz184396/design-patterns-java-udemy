package udemy.com.behavioral.strategy

import spock.lang.Specification

class StrategySpec extends Specification {

    def "calculate discriminant using ordinary discriminant strategy test when negative discriminant"() {

        given:
        double a = 3
        double b = 4
        double c = 3
        DiscriminantStrategy discriminantStrategy = new OrdinaryDiscriminantStrategy()

        when:
        double discriminantCalculated = discriminantStrategy.calculateDiscriminant(a,b,c)

        then:
        -20d == discriminantCalculated
    }
    // calculate discriminant using real discriminant strategy test when negative discriminant

    def "calculate discriminant using real discriminant strategy test when negative discriminant"() {

        given:
        double a = 3
        double b = 4
        double c = 3
        DiscriminantStrategy discriminantStrategy = new RealDiscriminantStrategy()

        when:
        double discriminantCalculated = discriminantStrategy.calculateDiscriminant(a,b,c)

        then:
        discriminantCalculated.isNaN() == true
    }

    void "positive test ordinary strategy"()
    {
        given:
        DiscriminantStrategy strategy = new OrdinaryDiscriminantStrategy()
        QuadraticEquationSolver solver = new QuadraticEquationSolver(strategy)

        when:
        Pair<Complex, Complex> results = solver.solve(1, 10, 16)

        then:
        new Complex(-2, 0) == results.first
        new Complex(-8, 0) == results.second
    }

    def "positive test real strategy"()
    {
        given:
        DiscriminantStrategy strategy = new RealDiscriminantStrategy()
        QuadraticEquationSolver solver = new QuadraticEquationSolver(strategy)

        when:
        Pair<Complex, Complex> results = solver.solve(1, 10, 16)

        then:
        new Complex(-2, 0) == results.first
        new Complex(-8, 0) == results.second
    }

    def "negative test real strategy"()
    {
        given:
        DiscriminantStrategy strategy = new RealDiscriminantStrategy()
        QuadraticEquationSolver solver = new QuadraticEquationSolver(strategy)

        when:
        Pair<Complex, Complex> results = solver.solve(1, 4, 5)

        then:
        results.first.isNaN() == true
        results.second.isNaN() == true
    }

    def "negative test ordinary strategy"()
    {
        given:
        OrdinaryDiscriminantStrategy strategy = new OrdinaryDiscriminantStrategy()
        QuadraticEquationSolver solver = new QuadraticEquationSolver(strategy)

        when:
        Pair<Complex, Complex> results = solver.solve(1, 4, 5)

        then:
        new Complex(-2, 1)  == results.first
        new Complex(-2, -1) == results.second
    }

}

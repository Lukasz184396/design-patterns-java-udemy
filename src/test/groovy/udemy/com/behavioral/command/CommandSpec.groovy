package udemy.com.behavioral.command

import spock.lang.Specification

class CommandSpec extends Specification{

    def "can deposit money to account"() {
        given:
        Command command = new Command(Command.Action.DEPOSIT, 100 )
        Account account = new Account()

        when:
        account.process(command)

        then:
        100 == account.balance
    }

   def "can not withdraw money from account"() {
        given:
        Command withdrawCommand = new Command(Command.Action.WITHDRAW, 100 )
        Account account = new Account()

        when:
        account.process(withdrawCommand)

        then:
        0 == account.balance
        withdrawCommand.success == false
    }

    def "can deposit and withdraw money from account"() {
        given:
        Command depositCommand = new Command(Command.Action.DEPOSIT, 1000 )
        Account account = new Account()

        when:
        account.process(depositCommand)

        then:
        1000 == account.balance
        depositCommand.success == true

        when:
        Command withdrawCommand = new Command(Command.Action.WITHDRAW, 600 );
        account.process(withdrawCommand);

        then:
        400 == account.balance
        withdrawCommand.success ==true
    }

}

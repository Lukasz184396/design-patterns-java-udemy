package udemy.com.behavioral.observer

import spock.lang.Specification

class ObserverSpec extends Specification {

    void "rat can attack a player"() {
        given:
        Game game = new Game()

        when:
        Rat rat = new Rat(game)

        then:
        1 == rat.attack
    }

    def "swarm of rats can attack a player with cumulative power"() {
        given:
        Game game = new Game()

        when:
        List<Rat> ratSwarm = new ArrayList<>()
        for (int i = 0; i < 10; i++) {
            Rat rat = new Rat(game)
            ratSwarm.add(rat)
        }

        then:
        10 == ratSwarm.get(0).attack
    }

    void "rat can die"() throws IOException {
        given:
        Game game = new Game()

        when:
        Rat rat = new Rat(game)
        rat.close()

        then:
        0 == rat.attack    //dead rat cannot attack
    }

}

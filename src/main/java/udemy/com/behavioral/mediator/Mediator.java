package udemy.com.behavioral.mediator;

import java.util.ArrayList;
import java.util.List;

public class Mediator {

    List<Participant> participants = new ArrayList<>();

    public void join(Participant participant) {
        participants.add(participant);
    }

    public void broadcast(Participant sender, int increasingValue) {
        for (Participant participant : participants) {
            participant.notify(sender, increasingValue);
        }
    }
}

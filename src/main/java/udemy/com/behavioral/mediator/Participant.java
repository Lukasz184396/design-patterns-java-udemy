package udemy.com.behavioral.mediator;

class Participant {

    int value = 0;
    Mediator mediator;

    public Participant(Mediator mediator) {
        this.mediator = mediator;
        mediator.join(this);
    }

    public void say(int increasingValue) {
        mediator.broadcast(this, increasingValue);
    }

    public void notify(Participant sender, int increasingValue) {
        if (sender != this)
            value += increasingValue;
    }

}

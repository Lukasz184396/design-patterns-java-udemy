package udemy.com.behavioral.visitor;

abstract class Expression
{
    abstract void accept(ExpressionVisitor ev);
}
package udemy.com.behavioral.visitor;

class Value extends Expression
{
    public int value;

    public Value(int value)
    {
        this.value = value;
    }

    @Override
    void accept(ExpressionVisitor ev)
    {
        // todo
        ev.visit(this);
    }
}

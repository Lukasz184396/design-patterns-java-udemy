package udemy.com.behavioral.strategy;

interface DiscriminantStrategy {
    double calculateDiscriminant(double a, double b, double c);
}

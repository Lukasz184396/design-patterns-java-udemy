package udemy.com.behavioral.chainofresponsibility;

public class StatQuery {

    public Statistic statistic;
    public int result;

    public StatQuery(Statistic statistic) {
        this.statistic = statistic;
    }
}

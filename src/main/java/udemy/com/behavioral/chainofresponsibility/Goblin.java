package udemy.com.behavioral.chainofresponsibility;

public class Goblin extends Creature {

    public Goblin(Game game) {
        // todo
        super(game);
        baseAttack = 1;
        baseDefense = 1;
    }

    public Goblin(Game game, int baseAttack, int baseDefense) {
        super(game, baseAttack, baseDefense);
    }

    @Override
    public int getAttack() {
        // todo
        StatQuery q = new StatQuery(Statistic.ATTACK);
        for (Creature c : game.creatures)
            c.query(this, q);
        return q.result;
    }

    @Override
    public int getDefense() {
        // todo
        StatQuery q = new StatQuery(Statistic.DEFENSE);
        for (Creature c : game.creatures)
            c.query(this, q);
        return q.result;
    }

    @Override
    public void query(Object source, StatQuery sq) {
        if (source == this) {
            switch (sq.statistic) {
                case ATTACK:
                    sq.result += baseAttack;
                    break;
                case DEFENSE:
                    sq.result += baseDefense;
                    break;
            }
        } else if (sq.statistic == Statistic.DEFENSE) {
            sq.result++;
        }
    }
}

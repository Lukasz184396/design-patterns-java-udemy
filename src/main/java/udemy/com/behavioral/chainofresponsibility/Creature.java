package udemy.com.behavioral.chainofresponsibility;

abstract class Creature {
    protected Game game;
    protected int baseAttack, baseDefense;

    public Creature(Game game, int baseAttack, int baseDefense) {
        this.game = game;
        this.baseAttack = baseAttack;
        this.baseDefense = baseDefense;
    }

    public Creature(Game game) {
        this.game = game;
    }

    public abstract int getAttack();

    public abstract int getDefense();

    public abstract void query(Object source, StatQuery sq);
}

package udemy.com.behavioral.state;

class CombinationLock {

    private int[] combination;
    public String status;

    private int indexOfEnteredDigit = 0;
    private boolean failed = false;

    public CombinationLock(int[] combination) {
        this.combination = combination;
        reset();
    }

    public void enterDigit(int digit) {
        if (status.equals("LOCKED")) status = "";
        status += digit;

        if (combination[indexOfEnteredDigit] != digit) {
            failed = true;
            status = "ERROR";
        }

        indexOfEnteredDigit++;

        if (indexOfEnteredDigit == combination.length)
            status = failed ? "ERROR" : "OPEN";
    }

    private void reset() {
        failed = false;
        status = "LOCKED";
        indexOfEnteredDigit = 0;
    }
}
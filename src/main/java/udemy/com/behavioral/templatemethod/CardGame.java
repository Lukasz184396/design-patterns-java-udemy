package udemy.com.behavioral.templatemethod;


abstract class CardGame {
    public Creature[] creatures;

    public CardGame(Creature[] creatures) {
        this.creatures = creatures;
    }

    // returns -1 if no clear winner (both alive or both dead)
    public int combat(int creature1, int creature2) {
        Creature first = creatures[creature1];
        Creature second = creatures[creature2];
        hit(first, second);
        hit(second, first);
        // todo: determine who won and return either creature1 or creature2
        boolean firstCreatureWins = first.health > 0 && second.health <= 0;
        boolean secondCreatureWins = second.health > 0 && first.health <= 0;
        if (firstCreatureWins)
            return creature1;
        else if (secondCreatureWins)
            return creature2;
        else return -1;
    }

    // attacker hits other creature
    protected abstract void hit(Creature attacker, Creature other);
}
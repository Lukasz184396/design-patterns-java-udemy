package udemy.com.behavioral.memento;

class Token {
    public int value = 0;

    public Token(int value) {
        this.value = value;
    }
}

package udemy.com.behavioral.memento;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

class TokenMachine {
    public List<Token> tokens = new ArrayList<>();


    public Memento addToken(int value) {
        return addToken(new Token(value));
    }

    public Memento addToken(Token token) {
        tokens.add(token);
        Memento m = new Memento();
        for (Token t : tokens)
            m.tokens.add(new Token(t.value));
        return m;
    }

    public void revert(Memento m) {
        tokens = m.tokens.stream()
                .map(t->new Token(t.value))
                .collect(Collectors.toList());
    }
}
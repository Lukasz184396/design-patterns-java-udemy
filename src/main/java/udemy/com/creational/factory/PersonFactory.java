package udemy.com.creational.factory;

class Person
{
    public int id;
    public String name;

    public Person(int id, String name)
    {
        this.id = id;
        this.name = name;
    }
}

public class PersonFactory {

    public int personId = 0;
    public Person createPerson(String name)
    {
        return new Person(personId++, name);
    }
}


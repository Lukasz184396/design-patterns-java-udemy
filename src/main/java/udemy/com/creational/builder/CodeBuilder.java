package udemy.com.creational.builder;


public class CodeBuilder
{
    private Class classInstance = new Class();

    public CodeBuilder(String className)
    {
        classInstance.setName(className);
    }

    public CodeBuilder addField(String name, String type)
    {
        classInstance.getFields().add(new Field(name, type));
        return this;
    }

    @Override
    public String toString() {
        return classInstance.toString();
    }
}


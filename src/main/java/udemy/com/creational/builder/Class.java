package udemy.com.creational.builder;

import java.util.ArrayList;
import java.util.List;

class Class
{
    private String name;
    private List<Field> fields = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        String lineSeparator = System.lineSeparator();
        builder.append("public class " + name).append(lineSeparator)
               .append("{").append(lineSeparator);
        for (Field filed : fields) {
            builder.append("  " +  filed).append(lineSeparator);
        }
        builder.append("}");
        return builder.toString();
    }
}

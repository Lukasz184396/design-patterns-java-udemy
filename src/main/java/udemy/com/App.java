package udemy.com;

import udemy.com.converter.CamelCaseToSentenceConverter;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        String toConvert = "calculateDiscriminantUsingRealDiscriminantStrategyTestWhenNegativeDiscriminant";
        System.out.println("\"" + CamelCaseToSentenceConverter.convert(toConvert) + "\"");

    }
}

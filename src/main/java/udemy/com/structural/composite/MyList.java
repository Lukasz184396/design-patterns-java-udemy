package udemy.com.structural.composite;

import java.util.ArrayList;
import java.util.Collection;

class MyList extends ArrayList<ValueContainer> {
    // please leave this constructor as-is
    public MyList(Collection<? extends ValueContainer> c) {
        super(c);
    }

    public int sum() {
        int result = 0;
        for (ValueContainer value : this) {
            for (int i : value) {
                result += i;
            }
        }
        return result;
    }
}

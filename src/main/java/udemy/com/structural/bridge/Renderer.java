package udemy.com.structural.bridge;

interface Renderer {
    public String whatToRenderAs();
}

package udemy.com.structural.proxy;

class ResponsiblePerson extends Person {

    private Person person;

    public ResponsiblePerson(Person person) {
        super(person.getAge());
        this.person = person;
    }

    public String drive() {
        if (person.getAge() < 16)
            return "too young";
        return super.drive();
    }

    public String drink() {
        if (person.getAge() < 18)
            return "too young";
        return super.drink();
    }

    public String drinkAndDrive() {
        return "dead";
    }

    public void setAge(int value) { person.setAge(value); }
    public int getAge() { return person.getAge(); }
}

package udemy.com.structural.flyweight;

import java.util.*;

class Sentence {
    private String[] words;
    private Map<Integer, WordToken> tokens = new HashMap<>();

    public Sentence(String ch) {
        words = words = ch.split(" ");
    }

    public WordToken getWord(int index) {
        WordToken wordToken = new WordToken();
        tokens.put(index, wordToken);
        return tokens.get(index);
    }

    @Override
    public String toString() {
        ArrayList<String> listOfWords = new ArrayList<>();
        for (int i = 0; i < words.length; ++i) {
            String word = words[i];
            if (tokens.containsKey(i) && tokens.get(i).capitalize)
                word = word.toUpperCase();
            listOfWords.add(word);
        }
        return String.join(" ", listOfWords);
    }

    class WordToken {
        public boolean capitalize;
    }
}

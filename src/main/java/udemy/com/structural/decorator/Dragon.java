package udemy.com.structural.decorator;

public class Dragon {

    private int age;
    private Bird bird = new Bird();
    private Lizard lizard = new Lizard();

    public Dragon(int age) {
        if (age < 0)
            throw new IllegalArgumentException("Age should equals positive number!!!");
        this.age = age;
        bird.age = age;
        lizard.age = age;
    }

    public String fly() {
        return bird.fly();
    }

    public String crawl()
    {
        return lizard.crawl();
    }

    public void setAge(int age)
    {
        if (age < 0)
            throw new IllegalArgumentException("Age should equals positive number!!!");
        this.age = bird.age = lizard.age = age;
    }
}
